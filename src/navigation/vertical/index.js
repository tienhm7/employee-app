import { Mail, Home } from 'react-feather'

export default [
  {
    id: 'employeeDash',
    title: 'Employee',
    icon: <Home />,
    navLink: '/dashboard/employee'
  },
  {
    id: 'secondPage',
    title: 'Second Page',
    icon: <Mail size={20} />,
    navLink: '/second-page'
  }
]
