// ** Auth Endpoints
export default {
  loginEndpoint: '/employee/login',
  registerEndpoint: '/employee/register',
  refreshEndpoint: '/employee/refresh-token',
  logoutEndpoint: '/employee/logout',

  // ** This will be prefixed in employeeorization header with token
  // ? e.g. Authorization: Bearer <token>
  tokenType: 'Bearer',

  // ** Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: 'accessToken',
  storageRefreshTokenKeyName: 'refreshToken'
}
