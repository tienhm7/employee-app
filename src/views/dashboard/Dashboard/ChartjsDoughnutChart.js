// ** Third Party Components
import { Doughnut } from 'react-chartjs-2'
import { Monitor, Tablet, ArrowDown, ArrowUp } from 'react-feather'

// ** Reactstrap Imports
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap'
import GetDescriptionDonut from './DescriptionDonut'

const ChartjsRadarChart = ({ result, total_time, tooltipShadow, successColorShade, warningLightColor, dangerColor, primary }) => {
  // ** Chart Options
  const options = {
    maintainAspectRatio: false,
    cutout: 60,
    animation: {
      resize: {
        duration: 500
      }
    },
    plugins: {
      legend: { display: false },
      tooltips: {
        callbacks: {
          label(context) {
            console.log(context)
            const label = context.label || ''
            if (label) {
              label += 'Ronak: '
            }
            if (context.parsed.y !== null) {
              label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y)
            }
            return label
          }
        },
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: tooltipShadow,
        backgroundColor: '#fff',
        titleFontColor: '#000',
        bodyFontColor: '#000'
      }
    }
  }
  const labels = []
  const datas = []
  const color = {offsiteTime:'text-primary', remoteTime:'text-warning', onsiteTime:'text-success', offTime:'text-danger'}
  let maxvalue = 0
  for (const key in result) {
    if (result[key] > maxvalue) {
      maxvalue = result[key]
    }
    labels.push(key),
      datas.push(result[key])
      
  }
  // ** Chart data
  const data = {
    datasets: [
      {
        labels: labels,
        data: datas,
        backgroundColor: [successColorShade, warningLightColor, dangerColor, primary],
        borderWidth: 0,
        pointStyle: 'rectRounded'
      }
    ]
  }

  return (
    <Card>
      <CardHeader className='d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column'>
        <CardTitle tag='h4'>Total Time By Work Type</CardTitle>
      </CardHeader>
      <CardBody>
        <div style={{ height: '275px' }}>
          <Doughnut data={data} options={options} height={275} />
        </div>
        <div className='description'>
          {
            Object.entries(result).map(([key, value]) => (
              <GetDescriptionDonut key={key} data={[key, value]} color={color[key]} total_time={total_time}></GetDescriptionDonut>))
          },
        </div>  
      </CardBody>
    </Card>
  )
}

export default ChartjsRadarChart
