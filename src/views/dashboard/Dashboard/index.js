// ** React Imports
import { Fragment, useContext, useState, useEffect } from 'react'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Reactstrap Imports
import { Row, Col } from 'reactstrap'

// ** Deom Charts
import BarChart from './ChartjsBarChart'
import DoughnutChart from './ChartjsDoughnutChart'
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useSkin } from '@hooks/useSkin'
import { Calendar } from 'react-feather'
import Flatpickr from 'react-flatpickr'

import axios from 'axios'

import moment from 'moment'


// ** Styles
import '@styles/react/libs/flatpickr/flatpickr.scss'

const ChartJS = () => {
  // ** Context, Hooks & Vars
  const [Result, setResult] = useState({})
  const [FromDate, setFromDate] = useState()
  const [ToDate, setToDate] = useState() 

  const setDate = (selectedDates) => {
    setFromDate(moment(selectedDates[0].toISOString()).format('YYYY-MM-DD'))
    
    if (selectedDates.length > 1) {
      setToDate(moment(selectedDates[1].toISOString()).format('YYYY-MM-DD'))
    }
  }


  const { colors } = useContext(ThemeColors),
    { skin } = useSkin(),
    labelColor = skin === 'dark' ? '#b4b7bd' : '#6e6b7b',
    tooltipShadow = 'rgba(0, 0, 0, 0.25)',
    gridLineColor = 'rgba(200, 200, 200, 0.2)',
    warningLightColor = '#FDAC34',
    successColorShade = '#28dac6',
    dangerColor = '#03fc49',
    primary = "#fc0307"

    useEffect(() => {
    axios.post('http://127.0.0.1:8000/api/statistic', {
      start_date: FromDate,
      end_date: ToDate,
      user_id: 1,
      model: 3
    })
      .then(
        function (axiosTestResult) {
          setResult(axiosTestResult.data)
          console.log(axiosTestResult.data)
        })
  }, [FromDate, ToDate])

  
  return (
    <Fragment>
      <Breadcrumbs title='Dashboard' data={[{ title: 'Employee' }, { title: '' }]} />
      <Row className='match-height'>
        <Col sm='12'>
          <Calendar size={18} />
          <Flatpickr
            className='form-control flat-picker bg-transparent border-0 shadow-none'
            options={{
              mode: 'range',
              // eslint-disable-next-line no-mixed-operators
              defaultDate: [new Date(), new Date(new Date().getFullYear(), new Date().getMonth(), 1)],
              dateFormat: 'd-m-Y'
            }}
            onChange={setDate}
          />
        </Col>
        <Col xl='7' sm='12'>
          {(Object.keys(Result).length !== 0) ? <BarChart result={Result} success={successColorShade} labelColor={labelColor} gridLineColor={gridLineColor} /> : ''
          }

        </Col>
        <Col lg='5' sm='12'>
          {(Object.keys(Result).length !== 0) ? <DoughnutChart result ={Result.work_type} total_time ={Result.total_time} tooltipShadow={tooltipShadow} successColorShade={successColorShade} warningLightColor={warningLightColor} dangerColor={dangerColor} color={colors.primary.main} primary={primary} /> : ''}
         
        </Col>
      </Row>
    </Fragment>
  )
}

export default ChartJS
