import {Circle} from 'react-feather'

function GetDescriptionDonut(props) {
    return (
        <div className='d-flex justify-content-between mt-3 mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={17} className={props.color} />
            <span className='fw-bold ms-75 me-10'>{props.data[0]} : </span>
            {(props.total_time) ? <span><strong>{ (props.data[1] / props.total_time * 100).toFixed(0)}</strong>  %</span> : <span> 0 %</span>
          }
            
          </div>
        </div>
    )
}

export default GetDescriptionDonut