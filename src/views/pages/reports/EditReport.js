import React, {useState, ChangeEvent, useEffect} from "react"
import axios from "axios"
import {Routes, Route, useParams} from 'react-router-dom'
import moment from "moment"
import Swal from "sweetalert2"

const AddTutorial = () => {
    const {id} = useParams()

    const initialTutorialState = {
        project_id: '',
        project_name: '',
        user_name: '',
        detail: '',
        day: '',
        time: '',
        position: ''
    }

    const [tutorial, setTutorial] = useState(initialTutorialState)

    const [submitted, setSubmitted] = useState(false)

    const handleInputChange = (event) => {

        const {name, value} = event.target
        setTutorial({...tutorial, [name]: value})
    }

    useEffect(() => {
        axios.get(`employee/reports/${id}`).then((res) => {
            const dayReport = moment(res.data.report.day).format('YYYY-MM-DD')
            setTutorial({
                project_id: res.data.report.project_id,
                project_name: res.data.report.reportData.project_name,
                user_name: res.data.report.reportData.username,
                position: res.data.report.reportData.position,
                detail: res.data.report.detail,
                time: res.data.report.time,
                day: dayReport
            })
        })
    }, [])

    const saveTutorial = () => {

        const data = {
            project_id: tutorial.project_id,
            project_name: tutorial.project_name,
            user_name: tutorial.user_name,
            detail: tutorial.detail,
            time: tutorial.time,
            day: tutorial.day
        }

        axios.put(`employee/reports/${id}`, data).then((res) => {
            console.log(res.data.error)
            console.log(res.data.messages)

            const messagesSuccess = res.data.messages
            const error = res.data.error
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
            if (messagesSuccess !== undefined) {
                Toast.fire({
                    icon: 'success',
                    title: messagesSuccess
                })
            } else {
                console.log(1)
                Toast.fire({
                    icon: 'warning',
                    title: error
                })
            }
        })
            .then((response) => {
                setTutorial({
                    project_id: response.data.project_id,
                    project_name: response.data.project_name,
                    user_name: response.data.user_name,
                    detail: response.data.detail,
                    time: response.data.time,
                    day: response.data.day
                })
                setSubmitted(true)
            })

            .catch((e) => {
                console.log(e.type)
            })
    }

    const newTutorial = () => {
        setTutorial(initialTutorialState)
        setSubmitted(false)
    }

    return (
        <div className="submit-form needs-validation">
            <div className="card">
                <div className="card-body">
                    {submitted ? (
                        <div>
                            <h4>You submitted successfully!</h4>
                            <button className="btn btn-success" onClick={newTutorial}>
                                Add
                            </button>
                        </div>
                    ) : (
                        <div className='row was-validated'>
                            <div className="col-md-4 col-12 was-validated">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>Project_id</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.project_id}
                                        onChange={handleInputChange}
                                        name="project_id"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>Project</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.project_name}
                                        onChange={handleInputChange}
                                        name="project_name"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>

                            </div>
                            <div className="col-md-4 col-12">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>User</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.user_name}
                                        onChange={handleInputChange}
                                        name="user_name"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>

                            </div>
                            <div className="col-md-4 col-12">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>Detail</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.detail}
                                        onChange={handleInputChange}
                                        name="detail"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>

                            </div>
                            <div className="col-md-4 col-12">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>Time</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.time}
                                        onChange={handleInputChange}
                                        name="time"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>

                            </div>
                            <div className="col-md-4 col-12">
                                <div className="form-group">
                                    <label htmlFor="title" className='form-label'>Day</label>
                                    <input
                                        type="date"
                                        className="form-control"
                                        id="title"
                                        required
                                        value={tutorial.day}
                                        onChange={handleInputChange}
                                        name="day"
                                    />
                                </div>
                                <div className="valid-feedback ">Valid.</div>
                                <div className="invalid-feedback ">Please fill out this field.</div>

                            </div>
                            <div className="mt-1">
                                <button type="submit" className='btn mb-1 mb-sm-0 mr-0 mr-sm-1 btn-primary'
                                        onClick={saveTutorial}>
                                    Update
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}

export default AddTutorial