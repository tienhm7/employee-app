import { lazy } from 'react'

const DashboardEmployee = lazy(() => import('../../views/dashboard/Dashboard'))
const SecondPage = lazy(() => import('../../views/dashboard/SecondPage'))

const DashboardRoutes = [
    {
        path: '/dashboard/employee',
        element: <DashboardEmployee />
    },
    {
        path: '/second-page',
        element: <SecondPage />
    }
]

export default DashboardRoutes
