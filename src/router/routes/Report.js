import {lazy} from 'react'

const ViewReport = lazy(() => import('../../views/pages/reports/EditReport'))

const ReportsRoutes = [
    {
        path: '/reports/EditReport/:id',
        element: <ViewReport/>
    }

]

export default ReportsRoutes
